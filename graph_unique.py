import time
import sys
sys.setrecursionlimit(10600)



#This function takes last element as pivot, places
#the pivot element at its correct position in sorted
#array, and places all smaller (smaller than pivot)
#to left of pivot and all greater elements to right
#of pivot

#start of sorting algorithm
def partition(arr, low_value, high_value):
    i = (low_value - 1)  # index of smaller element
    pivot = arr[high_value]  # pivot

    for j in range(low_value, high_value):

        # If current element is smaller than the pivot
        if arr[j] < pivot:
            # increment index of smaller element
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]

    arr[i + 1], arr[high_value] = arr[high_value], arr[i + 1]
    return (i + 1)


# The main function that implements QuickSort
# arr[] --> Array to be sorted,
# low  --> Starting index,
# high  --> Ending index

# Function to do Quick sort
def quickSort(arr, low_value, high_value):
    if low_value < high_value:
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low_value, high_value)

        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low_value, pi - 1)
        quickSort(arr, pi + 1, high_value)

#end of sorting algorithm


#Ask user to input graph files
first_file = input("Your first file path: ")



#Putting input data into array
with open(first_file, "r") as file:
    vertices = file.read()
    vertices = vertices.split()

    vertices = [x for x in vertices[0: len(vertices): 2]]

#removes \n symbol from output list
vertices = list(map(lambda s: s.strip(), vertices))


#creates a list of a unique values by converting into set structure
#and then again into list
vertices = list(set(vertices))
#print(vertices)


#sorting vertices
start = time.time()   #count execution time of sorting algorithm

vertices_array_lenth = len(vertices)
quickSort(vertices, 0, vertices_array_lenth - 1)

done_time = time.time() - start

#create a new file with sorted unique values
sorted_unique_values = open ('sorted_unique_values.txt', "w")
sorted_unique_values.write(str(vertices).replace('\\t',':'))


file.close()
sorted_unique_values.close()

number_of_elements = int(input("Enter number of elements you want to see: "))
print(list(vertices[i] for i in range(0, number_of_elements)))



#printing time that took to proceed sort
print("\nIt took",done_time , "seconds to sort your file!")

close = input()
#End of current script
