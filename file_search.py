import os

# inputs from user
file_path = input("Enter a directory: ")
file_extension = input("Enter an extension: ")

# array for storing files with proper extension
# using in searching function
file_list = []

alert = "There is no such extension.Check directory or file extension!"


# searching function
def searching_and_storing_function():
    for root, dirs, files in os.walk(file_path, topdown=True):
        for name in files:
            if name.endswith(file_extension):
                file_list.append(os.path.join(root, name))

    if not file_list:
        print(alert)
    else:
        for files in file_list:
            print(files)
        print("Everything is OK.Check your file!")

    new_file = open('extension_file.txt', "w", encoding="UTF-8")
    if file_list:
        for files in file_list:
            new_file.write(files + "\n")
    else:
        new_file.write(alert)


# end of function

searching_and_storing_function()


# External script execution
new_script = input("Paste a script: ")

try:
    os.system(new_script)
except FileNotFoundError as error:
    print(error)
except TypeError as error:
    print(error)
else:
    close = input("\nHit the Enter button to exit")
# end
