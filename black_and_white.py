try:
    from PIL import Image
except ImportError as error:
    print(error)
    close = input("Press Enter to exit...")
    raise

import os

# converting to B&W photos


def black_and_white(input_image_path,
                    output_image_path):
    color_image = Image.open(input_image_path)
    bw = color_image.convert('L')
    bw.save(output_image_path)


# reading text file with image path
image_source = input("Put a text-file with list: ")

try:
    with open(image_source, "r", encoding="UTF-8") as file:
        source = file.readlines()
        source = [line.rstrip('\n') for line in source]
except Exception as error:
    print(error)
    close = input("Press Enter to exit...")
# cutting file extension
# it should be done to add properly
# black_and_white phase to new photos title
extension_list = []
for x in range(0, len(source)):
    extension_list.append(os.path.splitext(source[x])[0])

# adding a file extension which was cut in previous step
file_extension = input("Enter file extension once again: ")

# executing a black&white function
for x in range(0, len(source)):
    if __name__ == '__main__':
        black_and_white(source[x],
                        (extension_list[x] +
                         "_black_and_white." + file_extension))


print("\nDone...")
# end of script
