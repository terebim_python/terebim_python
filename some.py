import random


def quickSort(myList):
    if myList == []:
        return []
    else:
        pivot = random.choice(myList)
        lesser = quickSort([x for x in myList[1:] if x < pivot])
        greater = quickSort([x for x in myList[1:] if x >= pivot])
        myList = lesser + [pivot] + greater
        return myList


first_file = input("Your second file path: ")

# Putting input data into array
with open(first_file, "r") as file:
    vertices = file.readlines()
    vertices = [line.rstrip('\n') for line in vertices]


quickSort(vertices)


number_of_elements = int(input("Enter number of elements you want to see: "))
print("Sorted vertices is:")
print(list(vertices[i] for i in range(0, number_of_elements)))


