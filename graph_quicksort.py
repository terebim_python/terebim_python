import time

# This function takes last element as pivot, places
# the pivot element at its correct position in sorted
# array, and places all smaller (smaller than pivot)
# to left of pivot and all greater elements to right
# of pivot

# start of sorting algorithm


def partition(arr, low_value, high_value):
    print("5")
    i = (low_value - 1)  # index of smaller element
    pivot = arr[high_value]  # pivot
    print("6")
    for j in range(low_value, high_value):

        # If current element is smaller than the pivot
        print("7")
        if arr[j] < pivot:
            # increment index of smaller element
            i = i + 1
            arr[i], arr[j] = arr[j], arr[i]
        print("8")
    arr[i + 1], arr[high_value] = arr[high_value], arr[i + 1]
    return (i + 1)



# The main function that implements QuickSort
# arr[] --> Array to be sorted,
# low  --> Starting index,
somelist = file.split
# high  --> Ending index

# Function to do Quick sort
def quickSort(arr, low_value, high_value):
    print("2")
    if low_value < high_value:
        # pi is partitioning index, arr[p] is now
        # at right place
        pi = partition(arr, low_value, high_value)
        print("3")
        # Separately sort elements before
        # partition and after partition
        quickSort(arr, low_value, pi - 1)
        quickSort(arr, pi + 1, high_value)
        print("4")
# end of sorting algorithm


# Ask user to input graph files
first_file = input("Your first file path: ")
second_file = input("Your second file path: ")

vertices = []
edges = []
# Putting input data into array
with open(first_file, "r") as file:
    vertices = file.readlines()
    vertices = [line.rstrip('\n') for line in vertices]


with open(second_file, "r") as file2:
    edges = file2.readlines()
    edges = [line.rstrip('\n') for line in edges]

# sorting vertices
start = time.time()   # count execution time of sorting algorithm

vertices_array_lenth = len(vertices)
print("1")
quickSort(vertices, 0, vertices_array_lenth - 1)

# sorting edges
edges_array_lenth = len(edges)
quickSort(edges, 0, edges_array_lenth - 1)

time_to_done = time.time() - start

# Ask user to enter number of elements
number_of_elements = int(input("Enter number of elements you want to see: "))

# prints data
print("Sorted vertices is:")
print(list(vertices[i] for i in range(0, number_of_elements)))

print("\nSorted Edges is: ")
print(list(edges[j] for j in range(0, number_of_elements)))

# printing time that took to proceed sort
print("\nIt took", time_to_done, "seconds to sort your file!")

close = input("Close")
# End of current script
